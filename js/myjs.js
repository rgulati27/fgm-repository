$(document).ready(function(){
    $("section.fourthSection .categoriesContainer .categoryGroup").hover(function(){
        $("section.fourthSection").find(".show").removeClass("show");
        $(this).addClass("show");
    });
    $("section.ninthSection.categories .flexDiv .boxContent").click(function(){
        $(this).parent().addClass("show");
    });
    $("section.ninthSection.categories .flexDiv .flexDivFull img").click(function(){
        $(this).parent().parent().removeClass("show");
    });
});


  $(window).scroll(function() {
    $('.content').removeClass('fixed');
    // second section starts here
    let hT1 = $('.secondSection').offset().top,
        hS1 = $('.thirdSection').offset().top,
        hH1 = $('.secondSection').outerHeight(),
        wH1 = $(window).height(),
        wS1 = $(this).scrollTop();
    if (wS1 > (hT1+hH1-wH1) && wS1 < (hS1+hH1-wH1)){
        $('.secondSection .content').addClass('fixed');
    }
    else{
      $('.secondSection .content').removeClass('fixed');
    }
    // second section ends here
    // fifth section starts here
    let hT2 = $('.fifthSection').offset().top,
        hS2 = $('.sixthSection').offset().top,
        hH2 = $('.fifthSection').outerHeight(),
        wH2 = $(window).height(),
        wS2 = $(this).scrollTop();
    if (wS2 > (hT2+hH2-wH2) && wS2 < (hS2+hH2-wH2)){
        $('.fifthSection .content').addClass('fixed');
    }
    else{
      $('.fifthSection .content').removeClass('fixed');
    }
    // fifth section ends here
    // eigth section starts here
    let hT3 = $('.eigthSection').offset().top,
        hS3 = $('.ninthSection').offset().top,
        hH3 = $('.eigthSection').outerHeight(),
        wH3 = $(window).height(),
        wS3 = $(this).scrollTop();
    if (wS3 > (hT3+hH3-wH3) && wS3 < (hS3+hH3-wH3)){
        $('.eigthSection .content').addClass('fixed');
    }
    else{
      $('.eigthSection .content').removeClass('fixed');
    }
    // eigth section ends here
    // tenth section starts here
    let hT4 = $('.tenthSection').offset().top,
        hS4 = $('.eleventhSection').offset().top,
        hH4 = $('.tenthSection').outerHeight(),
        wH4 = $(window).height(),
        wS4 = $(this).scrollTop();
    if (wS4 > (hT4+hH4-wH4) && wS4 < (hS4+hH4-wH4)){
        $('.tenthSection .content').addClass('fixed');
    }
    else{
      $('.tenthSection .content').removeClass('fixed');
    }
    // tenth section ends here
    // third section starts here
    let hT5 = $('.thirdSection').offset().top,
        hS5 = $('.fourthSection').offset().top,
        hH5 = $('.thirdSection').outerHeight(),
        wH5 = $(window).height(),
        wS5 = $(this).scrollTop();
    if (wS5 > (hT5+hH5-wH5) && wS5 < (hS5+hH5-wH5)){
        $('.thirdSection .sectionInner').addClass('fixed');
    }
    else{
      $('.thirdSection .sectionInner').removeClass('fixed');
    }
    // third section ends here
    // sixth section starts here
    let hT6 = $('.sixthSection').offset().top,
        hS6 = $('.seventhSection').offset().top,
        hH6 = $('.sixthSection').outerHeight(),
        wH6 = $(window).height(),
        wS6 = $(this).scrollTop();
    if (wS6 > (hT6+hH6-wH6) && wS6 < (hS6+hH6-wH6)){
        $('.sixthSection .sectionInner').addClass('fixed');
    }
    else{
      $('.sixthSection .sectionInner').removeClass('fixed');
    }
    // sixth section ends here
    // seventh section starts here
    let hT7 = $('.seventhSection').offset().top,
        hS7 = $('.eigthSection').offset().top,
        hH7 = $('.seventhSection').outerHeight(),
        wH7 = $(window).height(),
        wS7 = $(this).scrollTop();
    if (wS7 > (hT7+hH7-wH7) && wS7 < (hS7+hH7-wH7)){
        $('.seventhSection .sectionInner').addClass('fixed');
    }
    else{
      $('.seventhSection .sectionInner').removeClass('fixed');
    }
    // seventh section ends here
});

$(document).ready(function(){
    $("a.fullMapLink").click(function(){
        $("img.fullMapImage").toggleClass("show");
    });
});